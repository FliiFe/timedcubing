//3*3*3 Cube scramble generator, made by FliiFe

//List of possible moves, and suffixes. For example, D is a move, and D' is a move+suffix. Same for D2.
//'D' can be considered as move+suffix if the suffix is null (cf. suffixes[0]).
var moves = ['U', 'D', 'R', 'L', 'F', 'B'];
var suffixes = ['', "'", '2'];

/**
 * Generate a scramble.
 * @param {int} length - Length of the scramble.
 * @param {string} turnMetric - Turn metric used for the length. Can be HTM or QTM. If not specified, HTM is used.
 */
function generateScramble(length, turnMetric) {
    var previousMove = '';
    var scramble = '';
    for (var i = 0; i < length; i++) {
        var move = moves[randInt(0, moves.length)];
        var suffix = suffixes[randInt(0, suffixes.length)];
        while (move === previousMove) {
            move = moves[randInt(0, moves.length)];
        }
        if (suffix === '2' && turnMetric === 'QTM' && i === (length - 1)) {
            suffix = suffixes[randInt(0, suffixes.length - 1)];
        }
        scramble += move;
        scramble += suffix;
        scramble += ' ';
        if (suffix === '') scramble += ' ';
        previousMove = move;
        if (turnMetric === "QTM" && suffix === suffixes[2]) {
            i++;
        }
    }
    return scramble;
}

function randInt(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}
